QUADROMANIA Solver 
==================

When I was young, I could be quite annoying. So, one Christmas, my parents gave me a box of wooden building blocks, formed like the five Tetris bricks with each shape in a different color. The box came with instructions to puzzle animal shapes or rectangles using a certain number of blocks of given types. Furthermore, blocks of the same type/color were not allowed to be adjacent to each other (cornering was OK). This box kept me busy for the whole Christmas holidays and some time probably beyond. 

The game was actually quite addictive because putting away the game meant to have to put back the pieces into the box it came in, and this, of course, was just as difficult as solving yet another puzzle before putting them back.

Recently, I pulled out the game from the shelf and me and my wife played a few games (it's still exciting, seriously!) and then wanted to put back the pieces, which turned out to be more difficult than we had anticipated. Furthermore, the instruction sheet in the box also had nasty questions, such like "Can you find out if there is a solution to build shape XYZ from pieces ABC?" 

Given that I had just finished my PhD on Pentomino puzzles (which is the same game but with 12 pieces that are somewhat similar to Tetris bricks; and no, Pentomino was not the main part of the thesis), I decided to use my skills to build a generic solver Quadromania solver and here is the code for you to play with. 