import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class AchtMalAchtLLOOTTSS {

	long statesChecked;
	long startTime;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Spielfeld spielfeld = new Spielfeld(8, 8);
		List<Spielstein> spielsteine = Spielstein.listSpecificationToList("LLLLOOOOTTTTZZZZ");
		AchtMalAchtLLOOTTSS solver = new AchtMalAchtLLOOTTSS();
		solver.startTime = System.currentTimeMillis();
		Spielfeld solution = solver.solve(spielfeld, spielsteine);
		if (solution == null)
			System.out.println("Keine Lösung gefunden.");
		else
			System.out.println(solution.toString());
		System.out.println("Geprüfte Zustände: " + solver.statesChecked);
		System.out.println("Laufzeit (in ms): " + (System.currentTimeMillis() - solver.startTime));
	}
	
	void tick(Spielfeld feld) {
		statesChecked++;
		if (statesChecked % 10000 == 0) {
			System.out.println(statesChecked);
			System.out.println(feld.toString());
		}
	}
	
	/** 
	 * solves Quadromania using the following algorithm:
	 *  - given a partially filled board and a list of pieces to be put on the board
	 *  - find bottom left-most cell on the board 
	 *  - take first piece from list, 
	 *  - try to place piece on the board
	 */
	Spielfeld solve(Spielfeld feld, List<Spielstein> steine) {
		tick(feld);
		Cell c = feld.emptyCellBottomLeft();
		Set<Spielstein.SpielsteinTypes> testedTypes = EnumSet.noneOf(Spielstein.SpielsteinTypes.class);
		for (int steinIndex = 0; steinIndex < steine.size(); steinIndex++) { // try all pieces
			// make a list of stones that can be skipped later on 
			Spielstein stein = steine.get(steinIndex);
			if (!testedTypes.contains(stein.getType())) { // skip types that we've tried before
				testedTypes.add(stein.getType());
				List<Spielstein> uebrigeSteine = new ArrayList<Spielstein>(steine);
				uebrigeSteine.remove(stein);
				do { // try all rotations
					Spielfeld nextState = feld.assign(stein, c);
					if (nextState != null) {
						if (nextState.isComplete()) 
							return nextState;
						else {
							Spielfeld recursiveSolution = solve(nextState, uebrigeSteine);
							if (recursiveSolution != null) {
								assert recursiveSolution.isComplete();
								return recursiveSolution;
							}
						}
					}
					stein = stein.rotate();
				} while (stein != null);
			}
		}
		return null;
	}

}
