import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

class Spielstein {
	private final SpielsteinTypes type;
	private final int rotation;
	
	private static final int[][][] typeDefinitionVectors = {
		{{0, 0}, {0, 1}, {1, 0}, {1, 1}}, // O
		{{0, 0}, {0, 1}, {0, 2}, {1, 0}}, // L
		{{0, 0}, {0, 1}, {1, 1}, {1, 2}}, // Z
		{{0, 0}, {0, 1}, {0, 2}, {1, 1}}, // T
		{{0, 0}, {0, 1}, {0, 2}, {0, 3}}  // I
	};
	
	Spielstein(char c) {
		this(SpielsteinTypes.fromChar(c));
	}
	
	Spielstein(SpielsteinTypes type) {
		this.type =  type;
		this.rotation = 0;
	}
	
	Spielstein(Spielstein stein) {
		this.type = stein.type;
		this.rotation = stein.rotation + 1;
	}
	
	/** neu rotierte Variante des Steins, oder null wenn es keine mehr gibt */
	Spielstein rotate() {
		if (moreRotations()) {
			return new Spielstein(this);
		} else {
			return null;
		}
	}
	
	private boolean moreRotations() {
		return rotation < type.rotations() - 1;
	}
	
	/** 
	 * the quaromania constraint.
	 * two pieces are compatible (i.e., can cover two adjacent fields), if:
	 *  - they are identical, OR
	 *  - have different colors
	 */
	boolean isQuadromaniaCompatible(Spielstein stein) {
		return stein == this || stein.type != this.type;
	}
	
	char getTypeChar() {
		return type.toString().charAt(0);
	}
	
	SpielsteinTypes getType() {
		return type;
	}
	
	int[][] getPositions() {
		return positionVectorCache.get(type)[rotation];
	}

	private static Map<SpielsteinTypes,int[][][]> positionVectorCache;
	
	static {
		positionVectorCache = new EnumMap<SpielsteinTypes,int[][][]>(SpielsteinTypes.class);
		for (SpielsteinTypes type : SpielsteinTypes.values()) {
			int[][][] typeCache = new int[type.rotations()][][];
			positionVectorCache.put(type, typeCache);
			Spielstein stein = new Spielstein(type);
			int rotation = 0;
			do {
				typeCache[rotation] = stein.computePositions();
				stein = stein.rotate();
				rotation++;
			} while (stein != null);
		}
	}
	
	private int[][] computePositions() {
		int[][] positions = getCanonicalPosition();
		// mirror
		if (rotation >= 4) 
			positions = mirror(positions);
		// rotate
		for (int i = 0; i < rotation % 4; i++) {
			positions = rotateCCW(positions);
		}
		// assert that left-most bottom field is normalized to 0/0 
		return normalize(positions);
	}
	
	/** rotation of a position array */
	private int[][] mirror(int[][] positions) {
		int[][] mirroredPositions = new int[positions.length][2];
		for (int i = 0; i < positions.length; i++) { // for all fields
			mirroredPositions[i][0] = -1 * positions[i][0]; // mirror along the X axis
			mirroredPositions[i][1] = positions[i][1]; // nothing to be done for Y
		}
		return mirroredPositions;
	}
	
	/** rotation of a position array */
	private int[][] rotateCCW(int[][] positions) {
		int[][] rotatedPositions = new int[positions.length][2];
		for (int i = 0; i < positions.length; i++) { // for all fields
			rotatedPositions[i][0] = -1 * positions[i][1]; // X becomes old Y
			rotatedPositions[i][1] = positions[i][0]; // Y becomes negated old X
		}
		return rotatedPositions;
	}
	
	/** shift positions so that the left-most bottom position is 0/0 */
	private int[][] normalize(int[][] positions) {
		int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
		for (int i = 0; i < positions.length; i++) {
			if (positions[i][1] < minY) {
				minY = positions[i][1]; // remember new bottom
				minX = Integer.MAX_VALUE; // check again for left-most when in new bottom row
			} 
			if (positions[i][1] == minY) {
				minX = Math.min(minX, positions[i][0]);
			}
		}
		if (minX == 0 && minY == 0) // nothing to be done; skip creation of new array
			return positions;
		else {
			int[][] normalizedPositions = new int[positions.length][2];
			for (int i = 0; i < positions.length; i++) {
				normalizedPositions[i][0] = positions[i][0] - minX;
				normalizedPositions[i][1] = positions[i][1] - minY;
			}
			return normalizedPositions;
		}
	}
	
	private int[][] getCanonicalPosition() {
		//int[][] typeDefinition;
		switch (type) {
		case O: return typeDefinitionVectors[0]; 
		case L: return typeDefinitionVectors[1]; 
		case Z: return typeDefinitionVectors[2]; 
		case T: return typeDefinitionVectors[3]; 
		case I: return typeDefinitionVectors[4]; 
		default: throw new RuntimeException("unknown type " + type);
		}
	}
	
	public String toString() {
		return "type " + type + " with rotation " + rotation;
	}

	static List<Spielstein> listSpecificationToList(String s) {
		List<Spielstein> steine = new ArrayList<Spielstein>(s.length()); 
		for (int i = 0; i < s.length(); i++) {
			steine.add(new Spielstein(s.charAt(i)));
		}
		return steine;
	}
	
	enum SpielsteinTypes {
		O, L, Z, T, I;
		static SpielsteinTypes fromChar(char c) {
			assert (c == 'O' || c == 'L' || c == 'Z' || c == 'T' || c == 'I');
			switch (c) {
			case 'O': return O;
			case 'L': return L;
			case 'Z': return Z;
			case 'T': return T;
			case 'I': return I;
			default: throw new RuntimeException("unparseable character");
			}
		}
		
		int rotations() {
			switch (this) {
			case O: return 1; // no rotations 
			case I: return 2; // a single rotation
			case T: return 4; // rotations yes, but not flipping
			case Z: return 8; // flipping and rotating
			case L: return 8; // flipping and rotating
			default: throw new RuntimeException("unknown type");
			}
			
		}
	}
}
