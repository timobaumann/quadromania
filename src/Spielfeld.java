import java.util.Arrays;


public class Spielfeld {

	/** marks the cells that are part of the game */
	private boolean[][] cells;
	/** marks which of the cells that are part of the game are assigned already */
	private Spielstein[][] cellAssignment;

	Spielfeld(int xmax, int ymax) {
		cells = new boolean[ymax][xmax];
		for (int x = 0; x < xmax; x++)
			for (int y = 0; y < ymax; y++)
				cells[y][x] = true;
		cellAssignment = new Spielstein[ymax][xmax];
	}
	
	/** get a new copy */
	private Spielfeld(Spielfeld feld) {
		cells = new boolean[feld.cells.length][];
		cellAssignment = new Spielstein[feld.cellAssignment.length][];
		for (int y = 0; y < cells.length; y++) {
			cells[y] = Arrays.copyOf(feld.cells[y], feld.cells[y].length);
			cellAssignment[y] = Arrays.<Spielstein>copyOf(feld.cellAssignment[y], feld.cellAssignment[y].length);
		}
	}
	
	boolean isComplete() {
		for (int y = 0; y < cells.length; y++) {
			for (int x = 0; x < cells[y].length; x++) 
				if (cells[y][x] && cellAssignment[y][x] == null)
					return false;
		}
		return true;
	}
	
	/** try to assign piece to board state and return next state; or null if assignment is impossible */
	Spielfeld assign(Spielstein stein, Cell c) {
		boolean flagImpossibleAssignment = false;
		for (int[] position : stein.getPositions()) {
			if (!testCellAssignment(stein, c.x + position[0], c.y + position[1])) {
				flagImpossibleAssignment = true;
				break;
			}
		}
		if (flagImpossibleAssignment) {
			return null;
		} else { // assign piece to next state
			Spielfeld nextState = new Spielfeld(this);
			for (int[] position : stein.getPositions()) {
				nextState.cellAssignment[c.y + position[1]][c.x + position[0]] = stein;
			}
			return nextState;
		}
	}
	
	/** 
	 * test if cell X/Y can be filled with stein; specifically:
	 *  - this cell is part of the board,
	 *  - this cell is not yet taken
	 *  - the Quadromania-neighborhood rule is satisfied
	 * @param x the X-position in the field (NOT the canonical position of stein
	 * @param y as for X 
	 * @return whether the cell assignment is possible
	 */
	private boolean testCellAssignment(Spielstein stein, int x, int y) {
		if (y < 0 || y >= cells.length) // out of bounds (Y)
			return false;
		if (x < 0 || x >= cells[y].length) // out of bounds (X)
			return false;
		if (!cells[y][x]) // not part of the board (irregular board shapes)
			return false;
		if (cellAssignment[y][x] != null) // cell is already taken
			return false;
		// quadromania neighborhood rules:
		// cells adjacent to this one may not be filled by piece of same color (unless it's part of the same piece)
		return testQuadromaniaCompatibility(stein, x - 1, y) 
			&& testQuadromaniaCompatibility(stein, x + 1, y)
			&& testQuadromaniaCompatibility(stein, x, y - 1)
			&& testQuadromaniaCompatibility(stein, x, y + 1);
	}
	
	/** test the quadromania compatibility of stein with the board position x/y */
	private boolean testQuadromaniaCompatibility(Spielstein stein, int x, int y) {
		if (y < 0 || y >= cells.length) // out of bounds (Y)
			return true;
		if (x < 0 || x >= cells[y].length) // out of bounds (X)
			return true;
		if (!cells[y][x]) // not part of the board (irregular board shapes)
			return true;
		if (cellAssignment[y][x] == null) // cell is not yet taken
			return true;
		return stein.isQuadromaniaCompatible(cellAssignment[y][x]);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("  Spielfeld:\n");
		for (int y = cells.length - 1; y >= 0; y--) {
			for (int x = 0; x < cells[y].length; x++) {
				sb.append(fieldToString(x, y));
				sb.append(" ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	private char fieldToString(int x, int y) {
		if (!cells[y][x]) return ' ';
		if (cellAssignment[y][x] == null) 
			return '*';
		else
			return cellAssignment[y][x].getTypeChar();
	}
	
	/** find the bottom left-most empty cell on the board */
	Cell emptyCellBottomLeft() {
		for (int y = 0; y < cells.length; y++) {
			for (int x = 0; x < cells[y].length; x++) {
				if (cells[y][x] && cellAssignment[y][x] == null) {
					return new Cell(x, y);
				}
			}
		}
		return null;
	}
	
}
